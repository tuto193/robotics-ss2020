#!/usr/bin/env python
import rospy
from sensor_msgs.msg import Imu
from tf.transformations import euler_from_quaternion

COUNT_N = 50
global counter
def callback(data):
    quaternion = (data.orientation.x,
                  data.orientation.y,
                  data.orientation.z,
                  data.orientation.w)
    roll, pitch, yaw = euler_from_quaternion(quaternion)
    if counter == COUNT_N:
        rospy.loginfo("Got IMU-Data: (%f, %f, %f)", roll, pitch, yaw)
        counter = 0
    else:
        counter += 1

if __name__ == '__main__':
    rospy.init_node('imu')
    rospy.Subscriber("/imu_corrected", Imu, callback)
    rospy.spin()
