#include <ros/ros.h>
#include <ros/rate.h>
#include <geometry_msgs/TransformStamped.h>
#include <cstdio>
#include <tf2/LinearMath/Quaternion.h>
#include <sensor_msgs/Imu.h>

using namespace std;
using namespace ros;

void poseCallback(const sensor_msgs::Imu::ConstPtr& msg)
{
    tf::Quaternion q;
    double roll, pitch, yaw;
    tf::quaternionMsgToTF(msg->orientation, q);
    tf::Matrix3x3(q).getRPY(roll, pitch, yaw);
}

int main(int argc, char **argv)
{
    init(argc, argv, "broadcaster");
    auto rate = Rate(1);
    return 0;
}
