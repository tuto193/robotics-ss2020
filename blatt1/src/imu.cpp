#include <ros/ros.h>
#include <sensor_msgs/Imu.h>
#include <tf/transform_datatypes.h>

int count = 0;
const int COUNT_N = 10;
double rolls[COUNT_N], pitches[COUNT_N], yaws[COUNT_N];

double average_n(double *vals)
{
    double av = 0;
    for(int i = 0; i < COUNT_N; i++)
    {
        av += vals[i];
    }
    return av/COUNT_N;
}

double sample_variance_n(double* samples)
{
    double av = average_n(samples);
    double variance = 0;
    for(int i = 0; i < COUNT_N; i++)
    {
        variance += (samples[i] * samples[i]);
    }
    variance /= COUNT_N;
    return variance - (av * av);
}

void imuCallback(const sensor_msgs::Imu::ConstPtr& msg)
{
    tf::Quaternion q;
    double roll, pitch, yaw;
    tf::quaternionMsgToTF(msg->orientation, q);
    tf::Matrix3x3(q).getRPY(roll, pitch, yaw);
    if(count >= COUNT_N)
    {
        double X_r, X_p, X_y;
        double A_r, A_p, A_y;
        X_r = sample_variance_n(rolls);
        A_r = average_n(rolls);
        X_p = sample_variance_n(pitches);
        A_p = average_n(pitches);
        X_y = sample_variance_n(yaws);
        A_y = average_n(yaws);
        ROS_INFO("Got IMU-Data (avg): (%lf, %lf, %lf)", X_r, X_p, X_y);
        ROS_INFO("Got IMU-Data (variance): (%lf, %lf, %lf)", A_r, A_p, A_y);
        count = 0;
    }
    else
    {
        rolls[count] = roll;
        pitches[count] = pitch;
        yaws[count] = yaw;
        count++;
    }
}
int main(int argc, char **argv)
{
    ros::init(argc, argv, "imu");
    ros::NodeHandle n;
    ros::Subscriber sub = n.subscribe("/imu_corrected", 1, imuCallback);

    ros::spin();
    return 0;
}
