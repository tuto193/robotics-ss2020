# radio_ros --  ROS Example Project (Python/C++) 

Template for assignment 0.2 of the "Robotik" lecture 2020 at the University of Osnabrück. The architecture to be implemented is shown in the figure below:

![ex_radio](docs/radio_ros_simple.png)

# Building

To build this project use

```bash
catkin_make --pkg radio_ros
```

in the __workspace directory__.

You can also build all projects in the workspace by using just 

```bash
catkin_make
```

# Python implementation

Place all python sources in the `scripts` folder.
Make the main scripts executable using
```bash
chmod +x <python-node-name>.py
```
and add the shebang `#! /usr/bin/env python` at the first line of the file.

After build, a python-node can be launched using:

```bash
rosrun radio_ros <python-node-name>.py 
```

# C++ implementation

Place all c++ sources in the `src` folder.

Don't forget to create an executable in the `CMakeLists.txt`!

After build, the c++-node can be launched using:

```bash
rosrun radio_ros <c++-node-name> 
```

# Hints
To list the existing Topics use
```bash
rostopic list
```

To show the messages published on a specific Topic e.g. `music`:
```bash
rostopic echo music
```

Do not forget to source your workspace in each terminal window
```bash
source <workspace-directory>/devel/setup.bash
```
