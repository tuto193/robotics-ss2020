#include <algorithm>
#include <string>

#include <ros/ros.h>
#include <ros/rate.h>
#include <std_msgs/String.h>
#include <radio_ros/Music.h>

ros::Publisher pub;


void weather_callback(const std_msgs::String& weather)
{
  ROS_INFO_STREAM("Current weather: " << weather.data << "\n");
}

void music_callback(const radio_ros::MusicConstPtr& music)
{
  static std_msgs::String feedback;

  if (music->artist == std::string("Coldplay"))
  {
    feedback.data = "I don't like " + music->artist;
  }
  else
  {
    feedback.data = "I like " + music->artist;
  }

  pub.publish(feedback);
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "farmer");
  ros::NodeHandle n;

  ros::Subscriber weather_sub = n.subscribe("weather", 1, weather_callback);
  ros::Subscriber music_sub = n.subscribe("music", 1, music_callback);
  pub = n.advertise<std_msgs::String>("feedback", 1);

  auto rate = ros::Rate(5);
  ros::spin();
}

