#include <algorithm>

#include <ros/ros.h>
#include <ros/rate.h>
#include <radio_ros/Music.h>

using MusicList = std::array<radio_ros::Music, 2>;

MusicList generate_music_list()
{
  radio_ros::Music music1, music2;
  music1.title = "A Sky Full of Stars";
  music1.album = "Ghost Stories";
  music1.artist = "Coldplay";

  music2.title = "Highway to Hell";
  music2.album = "Highway to Hell";
  music2.artist = "AC/DC";

  return {music1, music2};
}

int main(int argc, char **argv)
{
  MusicList music_list = generate_music_list();

  ros::init(argc, argv, "dj");
  ros::NodeHandle n;

  ros::Publisher pub = n.advertise<radio_ros::Music>("music", 1);
  auto rate = ros::Rate(1);

  const auto& song = music_list[0];

  while(ros::ok())
  {
    std::random_shuffle(music_list.begin(), music_list.end());
    pub.publish(song);
    rate.sleep();
  }
}


