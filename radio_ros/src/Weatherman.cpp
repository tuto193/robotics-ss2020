#include <algorithm>

#include <ros/ros.h>
#include <ros/rate.h>
#include <std_msgs/String.h>

using WeatherData = std::array<std_msgs::String, 3>;

WeatherData generate_weather_data() {
  std_msgs::String msg1, msg2, msg3;
  msg1.data = "It's gonna be cloudy today!";
  msg2.data = "Rain everywhere!";
  msg3.data = "Sunny!";
  return {msg1, msg2, msg3};
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "weatherman");
  ros::NodeHandle n;

  WeatherData data = generate_weather_data();
  ros::Publisher pub = n.advertise<std_msgs::String>("weather", 1);
  auto rate = ros::Rate(0.5);

  const auto& forecast = data[0];

  while(ros::ok())
  {
    std::random_shuffle(data.begin(), data.end());
    pub.publish(forecast);
    rate.sleep();
  }
}

