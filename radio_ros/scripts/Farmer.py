#! /usr/bin/env python3
import rospy
from std_msgs.msg import String
from radio_ros.msg import Music
import random

feedback_pub = None

def weather_callback(msg):
    print("Current weather:", msg.data)

def music_callback(msg):
    feedback = String()

    if msg.artist == 'Coldplay':
        feedback.data = "I don't like {}".format(msg.artist)
    else:
        feedback.data = "I like {}".format(msg.artist)

    feedback_pub.publish(feedback)

if __name__ == '__main__':
    rospy.init_node('farmer')
    rospy.Subscriber('weather', String, weather_callback)
    rospy.Subscriber('music', Music, music_callback)
    feedback_pub = rospy.Publisher('feedback', String, queue_size=10)
    r = rospy.Rate(5)
    rospy.spin()